import mysql.connector
import json

class ConfigException(Exception):
    def __init__(self, *args, **kwargs):
        super(ConfigException, self).__init__(self, *args, **kwargs)

class DatabaseException(Exception):
    def __init__(self, *args, **kwargs):
        super(DatabaseException, self).__init__(self, *args, **kwargs)

class MySqlDb(object):
    instance = None

    def __init__(self):
        try:
            with open('config.json') as fp:
                config = json.load(fp)
        except Exception as e:
            raise ConfigException('Error parsing config file -- ' + str(e))

        try:
            self.cnx = mysql.connector.connect(**config)
            self.cursor = self.cnx.cursor()
        except Exception as e:
            raise DatabaseException('Error connecting to SQL database -- ' + str(e))

    def query(self, *args):
        try:
            self.cursor.execute(*args)
        except mysql.connector.Error as err:
            raise DatabaseException('Failed to execute query:\n\n{}\n{}\n{}\n{}'.format(
                                     queryString, '-' * 80, str(err), '-' * 80))
        return self.cursor

    @staticmethod
    def getInstance():
        if not MySqlDb.instance:
            MySqlDb.instance = MySqlDb()

        return MySqlDb.instance
