from models import BaseModel

class InfoModel(BaseModel):
    def __init__(self):
        super(InfoModel, self).__init__()

    def getCustomers(self):
        res = self.db.query("SELECT * FROM Customers")
        return res
