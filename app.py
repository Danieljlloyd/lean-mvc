#!/usr/bin/python
from routing import UriRouter
import sys

def application(environ, startResponse):
    status = '200 OK'
    
    try:
        router = UriRouter()
        responseBody = router.submit(environ['PATH_INFO'])
    except Exception as e:
        print str(e)
        return "Error: Server error." 

    responseHeaders = [
        ('Content-Type', 'text/plain'),
        ('Content-Length', str(len(responseBody)))
    ]

    startResponse(status, responseHeaders)
    
    return [ responseBody ]

def response_dummy(status, responseHeaders):
    pass

if __name__ == '__main__':
    environ = { 'PATH_INFO': sys.argv[1], 
                'REQUEST_METHOD': 'GET'   }
    response = application(environ, response_dummy) 
    print str(response)
