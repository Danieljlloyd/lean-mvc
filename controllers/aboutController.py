from controllers import BaseController

class AboutController(BaseController):
    def index(self):
        return "This is the about page."

    @staticmethod
    def implements(identifier):
        return "about" in identifier
