from controllers import BaseController

class ContactController(BaseController):
    def index(self):
        return "This is the contact page."

    @staticmethod
    def implements(identifier):
        return "contact" in identifier
