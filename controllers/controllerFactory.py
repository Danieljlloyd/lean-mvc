from controllers import *

class ControllerFactory(object):
    def create(self, identifier):
        for aController in BaseController.__subclasses__():
            if aController.implements(identifier):
                return aController()
