from controllers import BaseController
from models import InfoModel

class HomeController(BaseController):
    def __init__(self):
        self.infoModel = InfoModel()

    def index(self):
        customers = self.infoModel.getCustomers()
        
        for (identifier, firstname, lastname) in customers:
            print "Firstname: {}\tLastname: {}".format(firstname, lastname)

        return "This is the home page."

    @staticmethod
    def implements(identifier):
        return identifier is ""
