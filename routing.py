import os
from controllers import ControllerFactory

class UriRouter(object):
    def __init__(self):
        self.factory = ControllerFactory()	

    def submit(self, uri):
        if '/' in uri:
            uri = uri.lstrip('/')
            tokens = uri.split('/')
            if len(tokens) < 3:
                tokens += [ None ] * (3 - len(tokens))

            controllerString = tokens[0]
            methodString = tokens[1] or 'index'
            if tokens[2]:
                args = tokens[2:]
            else:
                args = None

        else:
            controllerString = uri.strip('/')
            methodString = 'index'
            args = []

        try:
            controller = self.factory.create(controllerString)
            method = getattr(controller, methodString)
        except Exception as e:
            print str(e)
            return 'Error 404: Page not found'

        if args:
            return method(*args)
        else:
            return method()
